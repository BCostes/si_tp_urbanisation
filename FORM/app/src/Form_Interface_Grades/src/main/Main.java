package main;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import model.Grade;
import model.Student;
import model.Teacher;

public class Main {

	private static final String config_file = "config.ini";
	

	/*static String input_teachers = "/home/costesb/Documents/04_Cours/TP/SI_TP_Urbanisation/FORM/db/teachers.db";
	static String input_students = "/home/costesb/Documents/04_Cours/TP/SI_TP_Urbanisation/FORM/db/students.db";
	static String input_grades = "/home/costesb/Documents/04_Cours/TP/SI_TP_Urbanisation/FORM/db/grades.db";
	static String output_grades = "/home/costesb/Documents/04_Cours/TP/SI_TP_Urbanisation/SIES/interfaces/output/grades.state";
	static String output_rejets = "/home/costesb/Documents/04_Cours/TP/SI_TP_Urbanisation/FORM/interfaces/rejets_notes.log";
*/
	
	public static void main(String[] args) {

		Path p_config = Paths.get(config_file);

		String input_teachers = null;
		String input_students = null;
		String input_grades = null;
		String output_grades = null;
		String output_rejets = null;

		try(BufferedReader br = Files.newBufferedReader(p_config)){
			// header
			String config_line ="";
			while ((config_line=br.readLine()) != null){
				String[] config = config_line.split("=");
				if(config[0].equals("grades_db")) {
					input_grades = config[1];
				}
				else if(config[0].equals("teachers_db")) {
					input_teachers = config[1];
				}
				else if(config[0].equals("students_db")) {
					input_students = config[1];
				}
				else if(config[0].equals("rejets_logs")) {
					output_rejets = config[1];
				}
				else if(config[0].equals("input_grades")) {
					output_grades = config[1];
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	
			
		if(input_teachers==null || input_students==null || output_grades==null ||
				input_grades==null || output_rejets==null) {
			System.out.println("Fichier de configuration incomplets : certains chemins ne sont pas correctement spécifiés");
			System.exit(-1);
		}


		Path p_teacher = Paths.get(input_teachers);
		Path p_students = Paths.get(input_students);
		Path p_grades_out = Paths.get(output_grades);
		Path p_grades_in = Paths.get(input_grades);
		Path p_log = Paths.get(output_rejets);
		

		String log ="";
		String grades_states="";

		if(!Files.exists(p_grades_out)) {
			try {
				Files.createFile(p_grades_out);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		//loading students
		List<Student> students = new ArrayList<Student>();
		try(BufferedReader br = Files.newBufferedReader(p_students)){
			// header
			String st = br.readLine();
			while ((st=br.readLine()) != null){
				String[] sts = st.split("\t");
				Student student = new Student();
				student.setId(sts[0]);
				student.setName(sts[1]);
				student.setFirstName(sts[2]);
				student.setBirthDate(sts[3]);
				students.add(student);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		//loading teachers
		List<Teacher> teachers = new ArrayList<Teacher>();
		try(BufferedReader br = Files.newBufferedReader(p_teacher)){
			// header
			String st = br.readLine();
			while ((st=br.readLine()) != null){
				String[] sts = st.split("\t");
				Teacher teacher = new Teacher();
				teacher.setId(sts[0]);
				teacher.setName(sts[1]);
				teacher.setFirstName(sts[2]);
				teachers.add(teacher);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		//loading  grades
		List<Grade> grades = new ArrayList<Grade>();
		try(BufferedReader br = Files.newBufferedReader(p_grades_in)){
			// header
			String st = br.readLine();
			while ((st=br.readLine()) != null){
				if(st.equals("")) {
					break;
				}
				String[] sts = st.split("\t");
				Grade grade = new Grade();
				Student student = null;
				for(Student s: students) {
					if(s.getId().equals(sts[0])) {
						student = s;
						break;
					}
				}
				//sinon on a trouvé l'étudiant
				grade.setStudent(student);
				Teacher teacher = null;
				for(Teacher s: teachers) {
					if(s.getId().equals(sts[1])) {
						teacher = s;
						break;
					}
				}
				// sinon on a trouvé l'enseignant
				grade.setTeacher(teacher);
				grade.setCourse(sts[2]);
				grade.setGrade(Double.parseDouble(sts[3]));
				grade.setDate(sts[5]);
				grade.setComment(sts[4]);
				grades.add(grade);

			}
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// input grades
		try(BufferedReader br = Files.newBufferedReader(p_grades_out)){
			// header
			grades_states = br.readLine()+"\n";
			String st="";
			while ((st=br.readLine()) != null){
				String[] sts = st.split(";");
				if(sts[7].equals("1")) {
					// déja chargé
					grades_states += st+"\n";
					continue;
				}
				Grade grade = new Grade();
				Student student = null;
				for(Student s: students) {
					if(s.getId().equals(sts[0])) {
						student = s;
						break;
					}
				}
				if(student == null) {
					//étudiant n'existe pas en bdd
					SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
					Date date = new Date(System.currentTimeMillis());
					log += formatter.format(date)+" : rejet note car étudiant " + sts[0]+ " n'existe pas dans la bdd\n";
					grades_states += st+"\n";
					continue;
				}
				//sinon on a trouvé l'étudiant
				grade.setStudent(student);
				Teacher teacher = null;
				for(Teacher s: teachers) {
					if(s.getId().equals(sts[1])) {
						teacher = s;
						break;
					}
				}
				if(teacher == null) {
					//enseignant n'existe pas en bdd
					SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd 'at' HH:mm:ss z");
					Date date = new Date(System.currentTimeMillis());
					log += formatter.format(date)+" : rejet note car enseignant " + sts[1]+ " n'existe pas dans la bdd\n";
					grades_states += st+"\n";
					continue;
				}
				// sinon on a trouvé l'enseignant
				grade.setTeacher(teacher);
				grade.setCourse(sts[2]);
				grade.setGrade(Double.parseDouble(sts[3]));
				grade.setDate(sts[4]);
				grade.setComment(sts[5]);

				grades_states += sts[0]+";"+sts[1]+";"+sts[2]+";"+sts[3]+";"+sts[4]+";"+sts[5]+";"+"M;1\n";

				// note déja existante ?
				boolean update_grade = false;
				for(Grade g : grades) {
					if(g.equals(grade)) {
						g.setGrade(grade.getGrade());
						g.setComment(grade.getComment());
						update_grade=true;
						break;
					}
				}
				if(update_grade) {
					continue;
				}
				//sinon on ajoute
				grades.add(grade);

			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		
		//Export des notes
		String output ="id_st	id_es	course	grade	comment	date\n";
		for(Grade grade : grades) {
			output += grade.toString()+"\n";
		}
		output = output.substring(0, output.length()-1);
		try(BufferedWriter bw = Files.newBufferedWriter(p_grades_in, StandardOpenOption.WRITE)){
			bw.write(output);
			bw.flush();
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		// gestion rejets
		if(!Files.exists(p_log)) {
			try {
				Files.createFile(p_log);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		if(!log.equals("")) {
			log = log.substring(0, log.length()-1);
			try(BufferedWriter bw = Files.newBufferedWriter(p_log, StandardOpenOption.APPEND)){
				bw.write("\n"+log);
				bw.flush();
				bw.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}


		// maj fichier export grades
		try(BufferedWriter bw = Files.newBufferedWriter(p_grades_out, StandardOpenOption.WRITE)){
			bw.write(grades_states);
			bw.flush();
			bw.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}
