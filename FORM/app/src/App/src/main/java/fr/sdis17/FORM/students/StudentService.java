package fr.sdis17.FORM.students;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.sdis17.FORM.Dao;

@Component
public class StudentService {
	
	@Autowired
	private Dao<Student> studentDao;
	
	public void save(Student student) {
		studentDao.save(student);
	}

}
