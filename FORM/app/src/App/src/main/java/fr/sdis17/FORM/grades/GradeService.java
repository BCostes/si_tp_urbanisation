package fr.sdis17.FORM.grades;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import fr.sdis17.FORM.Dao;
@Component
public class GradeService{
	
	@Autowired
	private Dao<Grade> gradeDao;
	
	public List<Grade> getAll() {
		return gradeDao.getAll();
	}

}
