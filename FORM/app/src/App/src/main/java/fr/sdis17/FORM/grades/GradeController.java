package fr.sdis17.FORM.grades;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
@RestController
public class GradeController { 
	
	@Autowired
	private GradeService gradeService;

	@GetMapping("/grades")
	public List<Grade> all() {
		return gradeService.getAll();
	}

}