package fr.sdis17.FORM.students;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class StudentController {
	
	@Autowired
	private StudentService studentService;

	@PostMapping("/student")
	public Student saveStudent(@RequestBody Student student) {
		studentService.save(student);
		return student;
	}

}