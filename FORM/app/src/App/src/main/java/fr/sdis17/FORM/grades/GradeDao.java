package fr.sdis17.FORM.grades;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import fr.sdis17.FORM.Dao;
import fr.sdis17.FORM.students.Student;
import fr.sdis17.FORM.teachers.Teacher;


@Component
public class GradeDao implements Dao<Grade>{

	@Override
	public void save(Grade  t) {
		Path p = Paths.get("../db/grades.db");
		if(!Files.exists(p)) {
			try {
				Files.createFile(p);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		List<Student> students = new ArrayList<Student>();
		try(BufferedReader br = Files.newBufferedReader(p)){
			// header
			String st = "";
			String header = br.readLine();
			if(header == null) {
				header = "id;name;firstname;birthdate";
			}
			while ((st=br.readLine()) != null){
				String[] sts = st.split("\t");
				Student student = new Student();
				student.setId(sts[0]);
				student.setName(sts[1]);
				student.setFirstName(sts[2]);
				student.setBirthDate(sts[3]);
				students.add(student);
			}
			boolean writeOk=false;
			try(BufferedWriter bw = Files.newBufferedWriter(p, StandardOpenOption.WRITE)){
				bw.write(header+"\n");
				int cpt=0;
				for(Student student : students) {
					if(student.equals(t)) {
						bw.write(t.toString());
						writeOk=true;
					}
					else {
						bw.write(student.toString());
					}
					cpt++;
					if(cpt != students.size()) {
						bw.write("\n");
					}
				}
				if(!writeOk) {
					if(!students.isEmpty()) {
						bw.write("\n");
					}
					bw.write(t.toString());
				}
				bw.flush();
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	}

	@Override
	public List<Grade> getAll() {
		Path p = Paths.get("../db/grades.db");
		List<Grade> grades = new ArrayList<Grade>();
		try(BufferedReader br = Files.newBufferedReader(p)){
			// header
			String st = br.readLine();
			while ((st=br.readLine()) != null){
				String[] sts = st.split("\t");
				Grade grade = new Grade();
				grade.setStudent(new Student());
				grade.getStudent().setId(sts[0]);
				grade.setTeacher(new Teacher());
				grade.getTeacher().setId(sts[1]);
				grade.setCourse(sts[2]);
				grade.setGrade(Double.parseDouble(sts[3]));
				grade.setComment(sts[4]);
				grade.setDate(sts[5]);
				grades.add(grade);
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return grades;

	}

}