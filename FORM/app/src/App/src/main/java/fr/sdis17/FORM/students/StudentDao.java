package fr.sdis17.FORM.students;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import fr.sdis17.FORM.Dao;

@Component
public class StudentDao implements Dao<Student>{

	@Override
	public void save(Student t) {
		Path p = Paths.get("../db/students.db");
		if(!Files.exists(p)) {
			try {
				Files.createFile(p);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		List<Student> students = new ArrayList<Student>();
		try(BufferedReader br = Files.newBufferedReader(p)){
			// header
			String st = "";
			String header = br.readLine();
			if(header == null) {
				header = "id\ttname\tfirstname\tbirthdate";
			}
			while ((st=br.readLine()) != null){
				String[] sts = st.split("\t");
				Student student = new Student();
				student.setId(sts[0]);
				student.setName(sts[1]);
				student.setFirstName(sts[2]);
				student.setBirthDate(sts[3]);
				students.add(student);
			}
			boolean writeOk=false;
			try(BufferedWriter bw = Files.newBufferedWriter(p, StandardOpenOption.WRITE)){
				bw.write(header+"\n");
				int cpt=0;
				for(Student student : students) {
					if(student.equals(t)) {
						bw.write(t.toString());
						writeOk=true;
					}
					else {
						bw.write(student.toString());
					}
					cpt++;
					if(cpt != students.size()) {
						bw.write("\n");
					}
				}
				if(!writeOk) {
					if(!students.isEmpty()) {
						bw.write("\n");
					}
					bw.write(t.toString());
				}
				bw.flush();
			}
			catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		
	}

	@Override
	public List<Student> getAll() {
		// TODO Auto-generated method stub
		return null;
	}

}
