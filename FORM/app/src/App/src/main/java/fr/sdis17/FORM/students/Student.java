package fr.sdis17.FORM.students;

public class Student {
	
	private String name;
	private String id;
	private String firstName;
	private String birthDate;
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Student() {
		super();
	}
	
	
	
	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getBirthDate() {
		return birthDate;
	}

	public void setBirthDate(String birthDate) {
		this.birthDate = birthDate;
	}

	@Override
	public int hashCode() {
		return this.getId().hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (!( o instanceof Student)) {
			return false;
		}
		Student s = (Student)o;
		if(s == this) {
			return true;
		}
		if(s.getId().equals(this.getId())) {
			return true;
		}
		return false;
	}
	
	@Override
	public String toString() {
		return this.getId()+"\t"+this.getName()+"\t"+this.getFirstName()+"\t"+this.getBirthDate();
	}

}
