package main;


import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

public class Main {

	private static final String config_file = "config.ini";
	
	public static void main(String[] args) {
		
		Path p_config = Paths.get(config_file);

		String input_teachers = null;
		String output_teachers = null;
	

		try(BufferedReader br = Files.newBufferedReader(p_config)){
			// header
			String config_line ="";
			while ((config_line=br.readLine()) != null){
				String[] config = config_line.split("=");
				if(config[0].equals("input_teachers")) {
					input_teachers = config[1];
				}
				else if(config[0].equals("teachers_db")) {
					output_teachers = config[1];
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		
		if(input_teachers==null || output_teachers==null ) {
			System.out.println("Fichier de configuration incomplets : certains chemins ne sont pas correctement spécifiés");
			System.exit(-1);
		}
			

		
		Path p = Paths.get(input_teachers);
		try(BufferedReader br = Files.newBufferedReader(p)){
			String line = br.readLine();
			String output = "id\tname\tfirstname\n";
			while((line=br.readLine())!=null) {
				System.out.println(line);
				if(line.equals("")) {
					break;
				}
				String[] line_s=line.split(";");
				output += line_s[0]+"\t"+line_s[1]+"\t"+line_s[2]+"\n";
			}
			output = output.substring(0,output.length()-1);
			Path p2 = Paths.get(output_teachers);
			if(!Files.exists(p2)) {
				try {
					Files.createFile(p2);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			try(BufferedWriter bw = Files.newBufferedWriter(p2, StandardOpenOption.WRITE)){
				bw.write(output);
				bw.flush();
				bw.close();
			}
			br.close();
			

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

}
