
#!/bin/bash
database='cours'
cd ../db
file='profs.db'
echo 'id;id_rh;type;nom;prenom;date_n;nationalite' > $file
id=0
psql -U postgres -h 127.0.0.1 -X -d $database -At --single-transaction \
    --set AUTOCOMMIT=off \
    --set ON_ERROR_STOP=on \
    -t \
    --field-separator ' ' \
    --quiet \
    -c "select id_rh, nom, prenom, date_n, nationalite from sies_enseignants" \
    | while read id_rh nom prenom date_n nationalite ; do
    id=$((id+1))

    echo "$id;$id_rh;2;$nom;$prenom;$date_n;$nationalite">> $file

sed '$d' $file
done
