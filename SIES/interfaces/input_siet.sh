
#!/bin/bash
input="input/etudiants.csv"
output="../db/eleves.db"
id=0
while IFS= read -r line
do
	id=$((id+1))
	if [ $id = 1 ]
	then
		echo "id;id_rh;nom;prenom" > $output
	else
		echo "$id;$line" >> $output
	fi
done < "$input"