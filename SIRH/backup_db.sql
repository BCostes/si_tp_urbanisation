--
-- PostgreSQL database dump
--

-- Dumped from database version 10.14 (Ubuntu 10.14-0ubuntu0.18.04.1)
-- Dumped by pg_dump version 10.14 (Ubuntu 10.14-0ubuntu0.18.04.1)

-- Started on 2020-09-03 15:06:52 CEST

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- TOC entry 1 (class 3079 OID 13049)
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- TOC entry 2973 (class 0 OID 0)
-- Dependencies: 1
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 199 (class 1259 OID 24600)
-- Name: adresses; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.adresses (
    id integer NOT NULL,
    num_voie character varying,
    nom_voie character varying,
    complement character varying,
    localite character varying,
    code_postal character varying(5)
);


ALTER TABLE public.adresses OWNER TO postgres;

--
-- TOC entry 198 (class 1259 OID 24598)
-- Name: adresses_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.adresses_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.adresses_id_seq OWNER TO postgres;

--
-- TOC entry 2974 (class 0 OID 0)
-- Dependencies: 198
-- Name: adresses_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.adresses_id_seq OWNED BY public.adresses.id;


--
-- TOC entry 201 (class 1259 OID 24626)
-- Name: pays; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.pays (
    id integer NOT NULL,
    nom_pays character varying
);


ALTER TABLE public.pays OWNER TO postgres;

--
-- TOC entry 200 (class 1259 OID 24624)
-- Name: pays_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.pays_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.pays_id_seq OWNER TO postgres;

--
-- TOC entry 2975 (class 0 OID 0)
-- Dependencies: 200
-- Name: pays_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.pays_id_seq OWNED BY public.pays.id;


--
-- TOC entry 203 (class 1259 OID 24667)
-- Name: personnels; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.personnels (
    id integer NOT NULL,
    matricule character varying(3),
    type integer,
    nom_famille character varying(52),
    prenom character varying(52),
    prenoms_autres character varying(104),
    naissance date,
    sexe character varying(1),
    nationalite integer,
    id_adresse integer,
    securite_sociale character varying(13)
);


ALTER TABLE public.personnels OWNER TO postgres;

--
-- TOC entry 202 (class 1259 OID 24665)
-- Name: personnels_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.personnels_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.personnels_id_seq OWNER TO postgres;

--
-- TOC entry 2976 (class 0 OID 0)
-- Dependencies: 202
-- Name: personnels_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.personnels_id_seq OWNED BY public.personnels.id;


--
-- TOC entry 197 (class 1259 OID 24592)
-- Name: scolarite; Type: TABLE; Schema: public; Owner: postgres
--

CREATE TABLE public.scolarite (
    id integer NOT NULL,
    id_etudiant integer,
    debut date,
    fin date
);


ALTER TABLE public.scolarite OWNER TO postgres;

--
-- TOC entry 196 (class 1259 OID 24590)
-- Name: scolarite_id_seq; Type: SEQUENCE; Schema: public; Owner: postgres
--

CREATE SEQUENCE public.scolarite_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE public.scolarite_id_seq OWNER TO postgres;

--
-- TOC entry 2977 (class 0 OID 0)
-- Dependencies: 196
-- Name: scolarite_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: postgres
--

ALTER SEQUENCE public.scolarite_id_seq OWNED BY public.scolarite.id;


--
-- TOC entry 204 (class 1259 OID 24673)
-- Name: sies_enseignants; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.sies_enseignants AS
 SELECT e.matricule AS id_rh,
    e.nom_famille AS nom,
    e.prenom,
    e.naissance AS date_n,
    "substring"((p.nom_pays)::text, 0, 2) AS nationalite
   FROM (public.personnels e
     JOIN public.pays p ON ((p.id = e.nationalite)))
  WHERE (e.type = 1);


ALTER TABLE public.sies_enseignants OWNER TO postgres;

--
-- TOC entry 205 (class 1259 OID 24677)
-- Name: siet_etudiants; Type: VIEW; Schema: public; Owner: postgres
--

CREATE VIEW public.siet_etudiants AS
 SELECT et.matricule AS matricule_etudiant,
    et.nom_famille AS nom,
    et.prenom,
    replace((et.prenoms_autres)::text, ' '::text, ', '::text) AS autres_prenoms,
    et.naissance AS date_naissance,
    concat(ad.num_voie,
        CASE
            WHEN (ad.complement IS NULL) THEN ''::text
            ELSE ' '::text
        END, COALESCE(ad.complement, ''::character varying), ', ', ad.nom_voie, ' ', ad.code_postal, ' ', ad.localite) AS adresse,
    p.nom_pays AS nationalite,
    s.debut AS entree_ensg,
    s.fin AS sortie_ensg
   FROM (((public.personnels et
     JOIN public.pays p ON ((p.id = et.nationalite)))
     LEFT JOIN public.adresses ad ON ((ad.id = et.id_adresse)))
     JOIN public.scolarite s ON ((s.id_etudiant = et.id)))
  WHERE (et.type = 2);


ALTER TABLE public.siet_etudiants OWNER TO postgres;

--
-- TOC entry 2823 (class 2604 OID 24603)
-- Name: adresses id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.adresses ALTER COLUMN id SET DEFAULT nextval('public.adresses_id_seq'::regclass);


--
-- TOC entry 2824 (class 2604 OID 24629)
-- Name: pays id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pays ALTER COLUMN id SET DEFAULT nextval('public.pays_id_seq'::regclass);


--
-- TOC entry 2825 (class 2604 OID 24670)
-- Name: personnels id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.personnels ALTER COLUMN id SET DEFAULT nextval('public.personnels_id_seq'::regclass);


--
-- TOC entry 2822 (class 2604 OID 24595)
-- Name: scolarite id; Type: DEFAULT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scolarite ALTER COLUMN id SET DEFAULT nextval('public.scolarite_id_seq'::regclass);


--
-- TOC entry 2961 (class 0 OID 24600)
-- Dependencies: 199
-- Data for Name: adresses; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.adresses (id, num_voie, nom_voie, complement, localite, code_postal) VALUES (1, '12', 'rue des Peupliers ', NULL, 'Champs-sur-Marne', '77420');
INSERT INTO public.adresses (id, num_voie, nom_voie, complement, localite, code_postal) VALUES (2, '1', 'avenue Gustave Effeil', 'bis', 'Lognes', '77185');
INSERT INTO public.adresses (id, num_voie, nom_voie, complement, localite, code_postal) VALUES (3, '3', 'rue du chat perché', NULL, 'Paris', '75011');
INSERT INTO public.adresses (id, num_voie, nom_voie, complement, localite, code_postal) VALUES (4, '148', 'boulevard de la Villette', NULL, 'Paris', '75019');
INSERT INTO public.adresses (id, num_voie, nom_voie, complement, localite, code_postal) VALUES (5, '128', 'boulevard Voltaire', NULL, 'Paris', '75011');
INSERT INTO public.adresses (id, num_voie, nom_voie, complement, localite, code_postal) VALUES (6, '3', 'allée de la Chine', NULL, 'Noisiel', '77186');
INSERT INTO public.adresses (id, num_voie, nom_voie, complement, localite, code_postal) VALUES (7, '11', 'impasse Rablais', 'ter', 'Evry', '91000');


--
-- TOC entry 2963 (class 0 OID 24626)
-- Dependencies: 201
-- Data for Name: pays; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.pays (id, nom_pays) VALUES (1, 'France');
INSERT INTO public.pays (id, nom_pays) VALUES (2, 'Chine');
INSERT INTO public.pays (id, nom_pays) VALUES (3, 'Italie');


--
-- TOC entry 2965 (class 0 OID 24667)
-- Dependencies: 203
-- Data for Name: personnels; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.personnels (id, matricule, type, nom_famille, prenom, prenoms_autres, naissance, sexe, nationalite, id_adresse, securite_sociale) VALUES (1, '001', 2, 'Martin', 'Camille', 'Gertrude Laure', '1996-10-01', 'F', 1, 1, '2961085149652');
INSERT INTO public.personnels (id, matricule, type, nom_famille, prenom, prenoms_autres, naissance, sexe, nationalite, id_adresse, securite_sociale) VALUES (2, '003', 2, 'Dewai', 'Fang', NULL, '1995-10-20', 'H', 2, 2, '1951099236547');
INSERT INTO public.personnels (id, matricule, type, nom_famille, prenom, prenoms_autres, naissance, sexe, nationalite, id_adresse, securite_sociale) VALUES (3, '101', 2, 'Medhane', 'Elias', 'Alem Abel', '1999-08-23', 'H', 1, 3, '1990875178541');
INSERT INTO public.personnels (id, matricule, type, nom_famille, prenom, prenoms_autres, naissance, sexe, nationalite, id_adresse, securite_sociale) VALUES (4, '111', 2, 'Mercier', 'Laura', 'Cosette', '1999-02-16', 'F', 1, 4, '2990244666529');
INSERT INTO public.personnels (id, matricule, type, nom_famille, prenom, prenoms_autres, naissance, sexe, nationalite, id_adresse, securite_sociale) VALUES (5, '901', 1, 'Botta', 'Sergio', NULL, '1973-03-01', 'H', 3, 5, '1730375147413');
INSERT INTO public.personnels (id, matricule, type, nom_famille, prenom, prenoms_autres, naissance, sexe, nationalite, id_adresse, securite_sociale) VALUES (6, '902', 1, 'Frichti', 'Manuel', NULL, '1972-11-21', 'H', 1, 6, '1721113166985');
INSERT INTO public.personnels (id, matricule, type, nom_famille, prenom, prenoms_autres, naissance, sexe, nationalite, id_adresse, securite_sociale) VALUES (7, '905', 1, 'Hott', 'Cécilia', NULL, '1981-08-12', 'F', 1, 7, '2810885322398');


--
-- TOC entry 2959 (class 0 OID 24592)
-- Dependencies: 197
-- Data for Name: scolarite; Type: TABLE DATA; Schema: public; Owner: postgres
--

INSERT INTO public.scolarite (id, id_etudiant, debut, fin) VALUES (2, 1, '2016-09-01', '2019-09-30');
INSERT INTO public.scolarite (id, id_etudiant, debut, fin) VALUES (3, 2, '2016-09-01', '2019-09-30');
INSERT INTO public.scolarite (id, id_etudiant, debut, fin) VALUES (4, 3, '2019-09-01', NULL);
INSERT INTO public.scolarite (id, id_etudiant, debut, fin) VALUES (5, 4, '2019-09-01', NULL);


--
-- TOC entry 2978 (class 0 OID 0)
-- Dependencies: 198
-- Name: adresses_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.adresses_id_seq', 7, true);


--
-- TOC entry 2979 (class 0 OID 0)
-- Dependencies: 200
-- Name: pays_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.pays_id_seq', 2, true);


--
-- TOC entry 2980 (class 0 OID 0)
-- Dependencies: 202
-- Name: personnels_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.personnels_id_seq', 7, true);


--
-- TOC entry 2981 (class 0 OID 0)
-- Dependencies: 196
-- Name: scolarite_id_seq; Type: SEQUENCE SET; Schema: public; Owner: postgres
--

SELECT pg_catalog.setval('public.scolarite_id_seq', 5, true);


--
-- TOC entry 2829 (class 2606 OID 24608)
-- Name: adresses adresses_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.adresses
    ADD CONSTRAINT adresses_pkey PRIMARY KEY (id);


--
-- TOC entry 2831 (class 2606 OID 24634)
-- Name: pays pays_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.pays
    ADD CONSTRAINT pays_pkey PRIMARY KEY (id);


--
-- TOC entry 2833 (class 2606 OID 24672)
-- Name: personnels personnels_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.personnels
    ADD CONSTRAINT personnels_pkey PRIMARY KEY (id);


--
-- TOC entry 2827 (class 2606 OID 24597)
-- Name: scolarite scolarite_pkey; Type: CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scolarite
    ADD CONSTRAINT scolarite_pkey PRIMARY KEY (id);


--
-- TOC entry 2834 (class 2606 OID 24683)
-- Name: scolarite scolarite_id_etudiant_fkey; Type: FK CONSTRAINT; Schema: public; Owner: postgres
--

ALTER TABLE ONLY public.scolarite
    ADD CONSTRAINT scolarite_id_etudiant_fkey FOREIGN KEY (id_etudiant) REFERENCES public.personnels(id);


-- Completed on 2020-09-03 15:06:52 CEST

--
-- PostgreSQL database dump complete
--

