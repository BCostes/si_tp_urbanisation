# LICENCE


<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/4.0/88x31.png" /></a><br />Ce(tte) œuvre est mise à disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/4.0/">Licence Creative Commons Attribution - Pas d’Utilisation Commerciale - Partage dans les Mêmes Conditions 4.0 International</a>.



# TP : urbanisation d'une architecture spaghetti

## Installation et configuration

### Montage de la base de données

Montage de la base de données : 
* Clic droit sur Databases -> Create -> Database
	* Database : **cours**
	* Sauvegarder
* Tools -> Query Tool
	* Ouvrir le fichier backup_db.sql situé dans le dossier SIRH
	* Exécuter le script (F5)


### Installation de Rabbitmq

Copier et exécuter les instructions suivantes : 

```shell
sudo apt-get install curl gnupg apt-transport-https -y
```
```shell
curl -1sLf "https://keys.openpgp.org/vks/v1/by-fingerprint/0A9AF2115F4687BD29803A206B73A36E6026DFCA" | sudo gpg --dearmor | sudo tee /usr/share/keyrings/com.rabbitmq.team.gpg > /dev/null
```
```shell
curl -1sLf https://github.com/rabbitmq/signing-keys/releases/download/3.0/cloudsmith.rabbitmq-erlang.E495BB49CC4BBE5B.key | sudo gpg --dearmor | sudo tee /usr/share/keyrings/rabbitmq.E495BB49CC4BBE5B.gpg > /dev/null
```
```shell
curl -1sLf https://github.com/rabbitmq/signing-keys/releases/download/3.0/cloudsmith.rabbitmq-server.9F4587F226208342.key | sudo gpg --dearmor | sudo tee /usr/share/keyrings/rabbitmq.9F4587F226208342.gpg > /dev/null
```
```shell
sudo tee /etc/apt/sources.list.d/rabbitmq.list <<EOF
```
```shell
## Provides modern Erlang/OTP releases
##
deb [signed-by=/usr/share/keyrings/rabbitmq.E495BB49CC4BBE5B.gpg] https://ppa1.novemberain.com/rabbitmq/rabbitmq-erlang/deb/ubuntu jammy main
deb-src [signed-by=/usr/share/keyrings/rabbitmq.E495BB49CC4BBE5B.gpg] https://ppa1.novemberain.com/rabbitmq/rabbitmq-erlang/deb/ubuntu jammy main

# another mirror for redundancy
deb [signed-by=/usr/share/keyrings/rabbitmq.E495BB49CC4BBE5B.gpg] https://ppa2.novemberain.com/rabbitmq/rabbitmq-erlang/deb/ubuntu jammy main
deb-src [signed-by=/usr/share/keyrings/rabbitmq.E495BB49CC4BBE5B.gpg] https://ppa2.novemberain.com/rabbitmq/rabbitmq-erlang/deb/ubuntu jammy main

## Provides RabbitMQ
##
deb [signed-by=/usr/share/keyrings/rabbitmq.9F4587F226208342.gpg] https://ppa1.novemberain.com/rabbitmq/rabbitmq-server/deb/ubuntu jammy main
deb-src [signed-by=/usr/share/keyrings/rabbitmq.9F4587F226208342.gpg] https://ppa1.novemberain.com/rabbitmq/rabbitmq-server/deb/ubuntu jammy main

# another mirror for redundancy
deb [signed-by=/usr/share/keyrings/rabbitmq.9F4587F226208342.gpg] https://ppa2.novemberain.com/rabbitmq/rabbitmq-server/deb/ubuntu jammy main
deb-src [signed-by=/usr/share/keyrings/rabbitmq.9F4587F226208342.gpg] https://ppa2.novemberain.com/rabbitmq/rabbitmq-server/deb/ubuntu jammy main
EOF
```
```shell
sudo apt-get update -y
```
```shell
sudo apt-get install -y erlang-base \
                        erlang-asn1 erlang-crypto erlang-eldap erlang-ftp erlang-inets \
                        erlang-mnesia erlang-os-mon erlang-parsetools erlang-public-key \
                        erlang-runtime-tools erlang-snmp erlang-ssl \
                        erlang-syntax-tools erlang-tftp erlang-tools erlang-xmerl

```
```shell
sudo apt-get install rabbitmq-server -y --fix-missing
```

### Installation du client python pika

``` shell
sudo pip install pika
```

## Préambule

On s'intéresse au système d'information de l’École Nationale des Sciences Géothermiques (ENSG).
La construction du SI s'est faîte de manière opportuniste sans aucune stratégie SI clairement établie : empilement d'applications pour répondre en urgence à l'émergence des besoins métier, connexions entre applications développées au coup par coup, aboutissant à une architecture façon plat de spaghetti" dont la cartographie est donnée ci-dessous :

![SI de l'ENSG](img/tp0.png)

Le nouveau DSI fraîchement nommé fait le constat suivant :
* imbrication inextricable des applications
* redondance des flux d'informations
* évolutions du SI coûteuses et risquées
* très forte dépendance aux éditeurs d'applications.

Une projet d'urbanisation du système d’information de l'école est donc lancé.

## 1. Objectif du TP

Ce TP a pour objectif de concevoir et d'implémenter l'architecture applicative cible du SI urbanisé.

### 1.1 Description des applications 

On se focalisera sur le sous-ensemble suivant du SI :

![Sous-ensemble du SI conséidéré pour le TP](img/tp.png)


#### A noter

**Pour des questions de simplicité, seule la base de données  SIRH a été implémentée.
Pour les autres applications, des fichiers aux extensions .db jouent le rôle des tables de la base de données.**

**L'identifiant fonctionnel d'un personnel (étudiant ou enseignant) est son *matricule*.**
Les identifiants "id" des tables sont des identifiants techniques propres aux applications.

Les fonctionnalités des applications n'ont pas toutes été développées - l'objectif étant d'urbaniser l'architecture inter-applicative (les interfaces).

#### SIRH

* Logiciel de gestion des ressources humaines 
	* Développé en Ruby (Rails)
	* Base de données Postgresql
	* Fonctionnalités principales :
		* Saisie des informations de tous les personnels (enseignants, étudiants, administratifs ...)
		* Suivie des carrières et GEPEC

Le diagramme entité-relation associé est le suivant :

![Diagramme entité-relation de SIRH](img/mcd_sirh.png)


**La base de données est accessible sur la VM du TP en lançant pgadmin et en se connectant à la base de données "cours".** :
* utilisateur *postgres*
* mot de passe *postgres*

#### SIET

* Portail étudiant
	* Développé en Python
	* Base de données Oracle
	* Fonctionnalités principales :
		* consultation des notes et plannings
		* justification des absences
		* communication avec les enseignants

Le diagramme entité-relation associé est le suivant :

![Diagramme entité-relation de SIET](img/mcd_siet.png)

**Il ne faut aucun retour chariot en fin de fichier pour tous les fichiers .db de SIET.**


L'application se trouve dans le répertoire SIET :
* /app : exécutable de l'application
* /db : base de données
* /interfaces : scripts des interfaces inter-applicatives

L'application peut être lancée via un terminal :

```
python3 siet.py
```
Toutes les fonctionnalités n'ont pas été implémentées.


#### SIES

* Portail enseignant
	* Développé en HTML/CSS/Javascript
	* Base de données MySQL
	* Fonctionnalités principales :
		* consultation des plannings
		* consultation des fiches de paie
		* saisie des notes et appréciations

Le diagramme entité-relation associé est le suivant :

![Diagramme entité-relation de SIES](img/mcd_sies.png)

**Il faut un retour chariot en fin de fichier pour tous les fichiers .db de SIES.**

L'application se trouve dans le répertoire SIES :
* /db : base de données
* /interfaces : scripts des interfaces inter-applicatives

#### FORM

* Logiciel de la formation continue et initiale
	* Développé en Java + Spring Boot
	* Base de données SQL Server
	* Fonctionnalités principales :
		* saisie des plannings
		* saisie des absences
		* consultation des notes et appréciations
		* gestion de la formation continue des enseignants

Le diagramme entité-relation associé est le suivant :

![Diagramme entité-relation de FORM](img/mcd_form.png)


**Il ne faut aucun retour chariot en fin de fichier pour tous les fichiers .db de FORM.**

L'application se trouve dans le répertoire FORM :
* /app : exécutable de l'application
* /db : base de données
* /interfaces : scripts des interfaces inter-applicatives


L'application peut être lancée via un terminal :

```
java -jar FORM-0.0.1-SNAPSHOT.jar
```

Aucune fonctionnalité utilisateur n'a été développée : le lancement de l'application permet simplement de démarrer le serveur d’application associé et d'appeler ses web services pour le bon fonctionnement des interfaces.

### 1.2 Description des interfaces inter-applicatives

Les interfaces suivantes seront considérées :


![Architecture inter-applicative](img/tp2.png)

On supposera que les demi-connecteurs s'exécutent via des tâches planifiées (cronjob, démon, ou tâches planifiées Windows).

#### Interface SIRH -> SIET

* Objet : envoi de la liste des étudiants
* Demi-connecteur sortant
	* Vue *siet_etudiants* dans la base de données *cours* de SIRH
* Demi-connecteur entrant
	* Script python *input_sirh.py* dans SIET/interfaces
	* Le connecteur interroge la vue *siet_etudiants* et met à jour le fichier de base de données db/etudiants.db (annule et remplace)

Pour exécuter le demi-connecteur entrant :

```
cd SIET/interface
python3 input_sirh.py
```

#### Interface SIRH -> SIES

* Objet : envoi de la liste des enseignants
* Demi-connecteur sortant
	* Vue *sies_enseignants* dans la base de données *cours* de SIRH
* Demi-connecteur entrant
	* Script shell *input_sirh.sh* dans SIES/interfaces
	* Le connecteur interroge la vue *sies_enseignants* et met à jour le fichier de base de données db/profs.db (annule et remplace)

Pour exécuter le demi-connecteur entrant :

```
cd SIES/interface
bash input_sirh.sh
```


#### Interface SIET -> SIES

* Objet : envoi de la liste des étudiants
* Demi-connecteur sortant
	* Script python *output_sies.py* dans SIET/interfaces
	* Le script écrit un fichier d'échange *etudiants.csv* au format csv dans *SIES/interfaces/input*
	* Le format du fichier est le suivant : *id_rh;nom;prenom*
		* *id_rh* est le matricule de l'étudiant dans le SIRH
* Demi-connecteur entrant
	* Script shell *input_siet.sh* dans SIES/interfaces
	* Le connecteur lit le fichier d'échange *etudiants.csv* et met à jour le fichier de base de données db/eleves.db (annule et remplace)

Pour exécuter le demi-connecteur sortant :

```
cd SIET/interface
python3 output_sies.py
```

Pour exécuter le demi-connecteur entrant :
```
cd SIES/interface
bash input_siet.sh
```


#### Interface SIET -> FORM

* Objet : envoi de la liste des étudiants
* Demi-connecteur sortant
	* Script python *output_form.py* dans SIET/interfaces
	* Le script charge chaque étudiant du fichier db/etudiants.db dans un objet **json** et appelle un service web de FORM
* Demi-connecteur entrant
	* Web service de type *POST* : *http://localhost:8080/student*
	* Le WS met à jour l'étudiant en paramètre dans le fichier de base de données db/students.db 

Pour exécuter le demi-connecteur sortant :

```
cd SIET/interface
python3 output_form.py
```

#### Interface SIES -> FORM : enseignants

* Objet : envoi de la liste des enseignants
* Demi-connecteur sortant
	* Script shell *output_form_profs.sh* dans SIES/interfaces
	* Le script écrit un fichier plat d'échange *teachers.txt* *SIES/interfaces/output*
	* Le format du fichier est le suivant : *id;nom;prenom*
		* *id* est le matricule de l'enseignant dans le SIRH
* Demi-connecteur entrant
	* Jar exécutable *import_sies_teachers.jar*
	* L'exécutable lit le fichier d'échange et met à jour le fichier de base de données db/teachers.db (annule et remplace)
	* Un fichier de configuration *config.ini* renseigne les différents chemins des fichiers :
		* grades_db : fichier de base de donnés des notes de FORM
		* teachers_db : fichier de base de donnés des enseignants de FORM
		* students_db : fichier de base de donnés des étudiants de FORM
		* input_grades : fichier d'échange des notes SIES -> FORM
		* input_teachers : fichier d'échange des enseignants SIES -> FORM
		* rejets_logs : fichier de description des rejets lors de l'intégration des notes dans FORM (traitement des erreurs)

Pour exécuter le demi-connecteur sortant :

```
cd SIES/interface
bash output_form_profs.sh
```

Pour exécuter le demi-connecteur entrant :

```
cd FORM/interface
java -jar import_sies_teachers.jar
```


#### Interface SIES -> FORM : notes

* Objet : envoi de la liste des notes
* Demi-connecteur sortant
	* Script shell *output_form_notes.sh* dans SIES/interfaces
	* Le script écrit un fichier de transfert *grades.state* *SIES/interfaces/output*
	* Le format du fichier est le suivant : *id_st;id_es;course;grade;comment;date;comment;action;import*
		* *id_st* : matricule étudiant dans le SIRH
		* *id_st* : matricule enseignant dans le SIRH
		* *action* : M pour modification, C pour création, S pour suppression. Tous les enregistrements seront traités comme des modifications ici
		* *import* : 1 si la note a été correctement intégrée dans FORM, 0 sinon
	* **Ce fichier ne doit pas être supprimé et l'entête doit être spécifiée** (elle n'est pas régénérée)
	* Ce fichier s'incrémente à chaque exécution du connecteur (les anciennes lignes sont conservées)
	* Toutes les notes sont envoyées à chaque exécution, avec le flag *import* = 0 initialement
* Demi-connecteur entrant
	* Jar exécutable *import_sies_grades.jar*
	* L'exécutable lit chaque ligne du fichier de transfert. Si *import*=1, la ligne est ignorée
	* Une ligne = une note est traitée pour être intégrée dans FORM. Si l'intégration échoue (enseignant ou étudiant non existant dans FORM), le rejet est décrit dans le fichier de log FORM/interfaces/rejets_logs.log
	* Si la note est correctement intégrée, la ligne correspondante dans *grades.state* est modifiée avec *import*=1.

Pour exécuter le demi-connecteur sortant :

```
cd SIES/interface
bash output_form_notes.sh
```

Pour exécuter le demi-connecteur entrant :

```
cd FORM/interface
java -jar import_sies_grades.jar
```



#### Interface FORM -> SIET

* Objet : envoi de la liste des notes
* Demi-connecteur sortant
	* Web service de type *GET* : *http://localhost:8080/grades*
	* Le WS fournit la liste des notes au format **json**
* Demi-connecteur entrant
	* Script python *input_form.py* dans SIET/interfaces
	* Le script transforme l'objet json en liste de note et met à jour le fichier de base de données db/notes.db (annule et remplace)

Pour exécuter le demi-connecteur entrant :

```
cd SIET/interface
python3 input_form.py
```


## 3. Première partie : test de l'environnement

Effectuez un test du bon fonctionnement des environnements et des interfaces inter-applicatives.

### Base de données de SIRH

Lancer pgadmin3 et essayez de vous connecter à la base de données *cours* sur le serveur *localhost*
Familiarisez vous avec les tables, les vues, et leur contenu.

### Serveur d'application de FORM

Démarrer le serveur d'application du logiciel FORM, pour que les services web appelés par les interfaces avec SIET fonctionnent :
```
cd FORM/app
java -jar FORM-0.0.1-SNAPSHOT.jar
```

### Tester les interfaces

Faire un test complet des interfaces inter-applicatives, dans le bon ordre :

* SIRH -> SIET
* SIRH -> SIES
* SIET -> SIES
* SIET -> FORM
* SIES -> FORM (enseignants)
* Créez quelques notes directement dans le fichier .db de SIES
* SIES -> FORM (notes)
* FORM -> SIET

La description du lancement des interfaces se trouve dans la section * Description des interfaces inter-applicatives*.

A chaque fois, vérifier que les fichiers intermédiaires et/ou les fichiers de base de données (.db) sont correctement remplis par l'exécution des interfaces.


## 4. Deuxième partie : conception de l'architecture cible

Quel type de composant introduire dans l’écosystème du SI pour découpler les applications ?

Faire un schéma de l'architecture cible en spécifiant :
* toutes les interfaces
	* sortantes et entrantes aux applications
	* sortantes et entrantes au nouveau composant
* les objets transitant par chaque interface
* les noms et type des structures intermédiaires (fichiers plats, vues, tables, web services, etc.)


## 5. Urbanisation du SI

Nous allons utiliser **Rabbitmq** pour implémenter l'architecture précédemment conçue.
Tous les demi-connecteurs depuis et vers le broker seront réalisés en python.

### 5.1 Prise en main de RabbitMQ

Réalisez les deux tutos suivants :

* https://www.rabbitmq.com/tutorials/tutorial-one-python.html
* https://www.rabbitmq.com/tutorials/tutorial-three-python.html

Les installations sont déjà faîtes sur leS VM (rabbitMQ, pika, etc.).

### 5.2 Industrialisation des interfaces

Créer l'arborescence suivante à la racine du dossier contenant les répertoires SIET, FORM et SIES :
* */MOM*
	* */input*
		* contient les demi-connecteur entrant au broker
		* */files*
			* contiendra les fichiers d'échange écrit par les applications et consommés par le broker
	* */output*
		* contient les demi-connecteur sortant au broker
		* */files*
			* contiendra les fichiers d'échange écrit par le broker et consommés par les applications	


Réalisez progressivement l'urbanisation du SI.
Vous utiliserez le patron "publish/subscribe" du broker.


#### 5.2.1 Industrialisation du flux "étudiants"

Urbanisez les interfaces :

* SIRH -> SIET
	* le demi-connecteur input_sirh.py* importe les étudiants dans SIET en interrogeant directement la vue *siet_etudiants* mise à disposition par FORM. Comme on souhaite découpler SIET et FORM il faut:
		* un connecteur SIRH -> "MOM" qui interroge cette vue et envoi des messages avec les informations des étudiants à toutes les applications qui en ont besoin dans un format pivot
		* créer une nouvelle base de données "MOM" dans laquelle le broker pourra écrire des données
		* créer une table *siet_etudiants* vide ayant exactement la même structure que la vue *siet_etudiants* de FORM
		* un connecteur "MOM" -> base de données qui pousse les données reçues par le broker dans la table *siet_etudiants* de la bdd MOM
		* modifier légèrement le demi-connecteur -> SIET *input_sirh.py* pour qu'il aille chercher les données étudiants dans la table nouvellement crée et non plus directement dans la vue de FORM.
* SIET -> SIES
* SIET -> FORM

#### 5.2.2 Industrialisation du flux "enseignants"

Urbanisez les interfaces :

* SIRH -> SIES
* SIES -> FORM

#### 5.2.3 Industrialisation du flux "notes"

Urbanisez les interfaces :

* SIES -> FORM
* FORM -> SIET

Vous pourrez utiliser le script python *json_2_db* situé dans le répertoire */SIET/interfaces* qui permet, à partir d'une liste d'objet JSON des notes, de mettre à jour le fichier de base de données *notes.db*. 

### 5.3 Conseils



#### Librairies python utiles

*  **json** : encodage / décodage d'objet JSON
	* https://docs.python.org/fr/3/library/json.html
* **sys** pour inclure un module ou une fonction python dans un répertoire différent du répertoire courant
	* https://askubuntu.com/questions/470982/how-to-add-a-python-module-to-syspath
* **requests** : pour faire des requpetes http
	* https://docs.python-requests.org/en/master/ 


#### Messages envoyés

Le plus simple est d'envoyer des messages au format **json** via le broker.
Ces objets doivent être encodés pour être envoyés par le broker : 

```python
import json

...

ma_liste=[]
ma_liste.append({'nom':'Toto'})
ma_liste.append({'nom':'Tata'})

message = json.dumps(ma_liste)
...
channel.basic_publish(exchange='XXX', routing_key='', body=message)
```

et décodés une fois reçus pour être exploités :

```python
import json

...
def callback(ch, method, properties, body):
	ma_liste = json.loads(body)

	for obj in ma_liste:
		print(obj['nom'])
```

#### Tâches de fond

Si aucun *consummer* n'écoute un *exchange* de type *fannout*, tous les messages envoyés seront perdus.
Il faut donc, avant d'exécuter un demi-connecteur entrant au broker (qui va donc envoyer des messages), exécuter tous les demi-connecteurs sortant (ceux qui consomment les messages envoyés).
Plutôt que d'ouvrir une multitude de terminaux, il est possible de lancer un script en tâche de fond avec &:

```
python mon_script.py &
```

La commande *jobs* permet de lister tous les scripts qui tournent en tâche de fond.

Pour stopper un script qui tourne en tâche de fond, après avoir identifier son numéro *x* avec *jobs* :

```
kill %x
```
Pour les stopper tous :

```
kill $(jobs -p)
```

Pour passer en tâche de fond un script qui n'a pas été lancé avec & :

Faire *Ctrl+Z*
Puis :

```
bg
```
