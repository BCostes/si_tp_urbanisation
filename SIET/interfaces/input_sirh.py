#!coding utf-8
import psycopg2

with psycopg2.connect(
    host="localhost",
    database="cours",
    user="postgres",
    password="postgres") as conn:

	with conn.cursor() as curs:
		curs.execute("SELECT * FROM siet_etudiants;")
		with open("../db/etudiants.db", "w") as f:
			f.write('id;matricule_etudiant;nom;prenom;autres_prenoms;date_naissance;adresse;nationalite;entree_ensg;sortie_ensg\n')
			id=0
			for record in curs:
				id+=1
				s = str(id)
				for data in record:
					if data == None:
						s += ";" 
					else:
						s += ";" + str(data)
				if id != curs.rowcount:
					s += '\n'
				f.write(s)
		
