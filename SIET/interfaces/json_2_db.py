#!coding utf-8

import sys
sys.path.insert(1, '../app')
from db_connect import *

def load_json_2_db(grades, students_f, grades_f):
	students = load_students_from_db(students_f)

	with open(grades_f, "w") as f:
		f.write("id;id_etudiant;cours;note;date\n")
		id=0
		for grade in grades:
			
			id_etu=""
			for student in students:
				if student["matricule_etudiant"] == grade["student"]["id"]:
					id_etu = student["id"]
					break
			if id_etu == "":
				continue
			id+=1
			f.write(str(id)+";"+id_etu+";"+grade["course"]+";"+str(grade["grade"])+";"+grade["date"])
			if id != len(grades):
				f.write('\n')
