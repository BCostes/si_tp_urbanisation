#!coding utf-8

f_transfert = '../../SIES/interfaces/input/etudiants.csv'

with open('../db/etudiants.db') as file:
	output = "id_rh;nom;prenom\n"
	file.readline()
	for line in file:
		data = line.split(";")
		output+=data[1]+";"+data[2]+";"+data[3]+"\n"

	with open(f_transfert, 'w') as fout:
		fout.writelines(output)