#!coding utf-8

"""
	Load students from etudiants.db into list of dictionnaries
"""
def load_students_from_db(students_f):
	students = []
	with open(students_f) as file:
		# header
		head = file.readline()
		head=head.rstrip()
		keys = head.split(";")

		for line in file:
			line = line.rstrip()
			student_att = line.split(";")
			student = dict(zip(keys, student_att))
			students.append(student)
	return students

"""
	Load grades from notes.db into dictionnary where each entry is a student with grades as a list of tuples
"""
def load_grades_from_db(grades_f):
	grades = {}
	with open(grades_f) as file:
		# header
		head = file.readline()
		head=head.rstrip()
		keys = head.split(";")

		for line in file:
			line = line.rstrip()
			grades_att = line.split(";")
			id_strudent = grades_att[1]
			grade= (grades_att[2],grades_att[3],grades_att[4])
			if id_strudent in grades:
				grades[id_strudent].append(grade)
			else:
				student = [grade]
				grades[id_strudent] = student
	return grades


def get_student_by_id(students,id):
	for s in students:
		if s["id"] == id:
			return s;
	return None
