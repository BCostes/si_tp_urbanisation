#!coding utf-8
from db_connect import *

if __name__ == "__main__":
	students = load_students_from_db("../db/etudiants.db")
	grades = load_grades_from_db("../db/notes.db")

	students_id = [s['id'] for s in students]

	print("\t##########################################")
	print("\t##########################################")
	print("\t   Application de gestion des étudiants")
	print("\t##########################################")
	print("\t##########################################")
	print("\n\n\n")

	while True:
		k = input('>>> \'l\' : liste étudiants ; \'q\' : quitter\n')
		if k == 'q':
			break
		if k == 'l':
			while True:
				print('\n')
				print("\t##########################################")
				print("\t\t  Liste des étudiants")
				print("\t##########################################")
				print("\n")
				print("id : nom prénom")
				print("\n")
				for student in students:
					print(student["id"] + " : " + student["nom"] + " "+ student["prenom"])
				print("\n")
				kk = input('>>> \'id\' : détail étudiant id ; \'q\' : retour\n')
				kk=str(kk)
				if kk == 'q':
					break
				if kk in students_id:
					student = get_student_by_id(students, kk)
					while True:
						print('\n')
						print("\t##########################################")
						print("\t\t\t  Détail étudiant")
						print("\t##########################################")
						print("\n")
						for k in student:
							print(k + " : " + student[k])
		
						print("\n")
						kkk = input('>>> \'n\' : voir notes ; \'q\' : retour\n')
						if kkk == 'q':
							break
						if kkk == "n":
							grades_s = grades[kk]
							while True:
								print('\n')
								print("\t##########################################")
								print("\t\t\t  Notes étudiant")
								print("\t##########################################")
								print("\n")
								print(student["nom"] + " " + student["prenom"])
								print("\n")
								for grade in grades_s:
									print('----- ' + grade[2] + ' -----')
									print(grade[0] + " : " + grade[1])
								kkkk = input('>>> \'q\' : retour\n')
								if kkkk == 'q':
									break
